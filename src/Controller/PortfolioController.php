<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Project;
use App\Entity\Contact;
use App\Form\ContactType;
use App\Notification\ContactNotification;
use App\Repository\ImageRepository;
use App\Repository\ProjectRepository;
use Symfony\Component\HttpFoundation\Request;

// use App\Form\ContactType;

class PortfolioController extends AbstractController
{
    /**
     * @Route("/portfolio", name="portfolio")
     */
    public function index(ProjectRepository $repo)
    {
        $projects = $repo->findAll();
        return $this->render('portfolio/index.html.twig', [
            'controller_name' => 'PortfolioController',
            'projects' => $projects,
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home(ProjectRepository $repo, Request $request, ContactNotification $notification){

        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form ->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $notification->notify($contact);
            $this->addFlash('success', 'Votre Email a bien été envoyé');
            return $this->redirectToRoute('home');
        }

        $projects = $repo->findAll();

        
        return $this->render('portfolio/home.html.twig', [
            'projects' => $projects,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/project/{id}" , name="project")
     */
    public function show(Project $project, ImageRepository $repo, int $id){

        $images = $repo->findBy(['project' => $id]);

        return $this->render('portfolio/show.html.twig', [
            'project' => $project,
            'images' => $images
        ]);
    }
}
