<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Project;
use App\Entity\Image;
use App\Form\ProjectType;
use App\Repository\ImageRepository;
use App\Repository\ProjectRepository;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(ProjectRepository $repo)
    {
        $projects = $repo->findAll();

        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'projects' => $projects,
        ]);
    }

    /**
     * @Route("/admin/projet/edit", name="edit_project")
     */
    public function edit(){
        return $this->render('admin/edit.html.twig');
    }

    /**
     * @Route("/admin/project/new", name="new_project")
     * @Route("/admin/project/{id}/edit", name="edit_project")
     */
    public function create(Project $project = null , Image $image = null, ImageRepository $repo, Request $request, ObjectManager $manager, int $id = null){
    
        if(!$project){
            $project = new Project();
        }

        if(!$image){
            $image = new Image();
        }

        $form = $this->createForm(ProjectType::class, $project);   
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            // var_dump($request->files);
            // exit();

                $cardFile = $form['imageCard']->getData();
                $imageFiles = $form['images']->getData();

                if($cardFile){
                    $originalFilename = pathinfo($cardFile->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()' , $originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$cardFile->guessExtension();
    
                    try{
                        $cardFile->move(
                            $this->getParameter('card_directory'),
                            $newFilename
                        );
                    }catch (FileException $e){
    
                    }
                    $project->setImageCard($newFilename);
                }

                if($imageFiles){
                    foreach ($imageFiles as $imageFile){
                        $originFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()' , $originFilename);
                        $fileName = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();

                        try{
                            $imageFile->move(
                                $this->getParameter('imageProject_directory'),
                                $fileName
                            );
                        }catch(FileException $e){

                        }
                        
                        $image->setName($fileName);
                        $image->setProject($project);
                        $manager->persist($image);
                    }
                }      
            $manager->persist($project);
            $manager->flush();
            $this->addFlash('success', 'le projet a bien été modifié');

            return $this->redirectToRoute('project', ['id' => $project->getId()]);
        }

        $images = $repo->findBy(['project' => $id]);

        return $this->render('admin/new.html.twig', [
            'images' => $images,
            'project' => $project,
            'formProject' => $form->createView(),
            'editMode' => $project->getId() !== null,
        ]);
    }

    /**
     * @Route("/admin/project/{id}/delete", name="delete_project")
     */
    public function delete(ProjectRepository $repo, ImageRepository $repoImage , ObjectManager $manager, int $id){
    

        $project = $repo->find($id);
        $images = $repoImage->findBy(['project' => $id]);
        $manager->remove($project);

        foreach($images as $image){
            $project->removeImage($image);
            $manager->remove($image);
        }
        $manager->flush();
        $this->addFlash('success', 'le projet a bien été supprimé');

        return $this->redirectToRoute('admin');
    }
}
