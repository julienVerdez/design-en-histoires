<?php 
namespace App\Notification;

use App\Entity\Contact;
use Twig\Environment;

class ContactNotification{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $renderer;

    public function __construct(\Swift_Mailer $mailer, Environment $renderer )
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    public function notify(Contact $contact){
        $message = (new \Swift_Message('Design en histoire :'))
            ->setFrom($contact->getEmail())
            ->setTo('julien.verdez@gmail.com')
            ->setBody($contact->getMessage(), 'text/html');

        $this->mailer->send($message);
    }
}