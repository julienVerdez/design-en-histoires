<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190726070831 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE4ACC9A20');
        $this->addSql('DROP TABLE card');
        $this->addSql('DROP INDEX UNIQ_2FB3D0EE4ACC9A20 ON project');
        $this->addSql('ALTER TABLE project ADD subtitle VARCHAR(255) NOT NULL, ADD image_card VARCHAR(255) NOT NULL, ADD section_content VARCHAR(255) NOT NULL, ADD footer_project VARCHAR(255) NOT NULL, DROP card_id, CHANGE images image_project LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE card (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, subtitle VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, image_card VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE project ADD card_id INT NOT NULL, DROP subtitle, DROP image_card, DROP section_content, DROP footer_project, CHANGE image_project images LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE4ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2FB3D0EE4ACC9A20 ON project (card_id)');
    }
}
