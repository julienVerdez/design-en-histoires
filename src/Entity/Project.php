<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min = "5", 
     *               max = "255", 
     *               minMessage = "Ce champs doit faire au moins 5 caractères.",
     *               maxMessage = "Ce champs ne peut dépasser 255 caractères.", )
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="5", 
     *         max="255", 
     *         minMessage="Ce champs doit faire au moins 5 caractères.",
     *         maxMessage="Ce champs ne peut dépasser 255 caractères.", )
     */
    private $subtitle;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="5", 
     *         max="255", 
     *         minMessage="Ce champs doit faire au moins 5 caractères.",
     *         maxMessage="Ce champs ne peut dépasser 255 caractères.", )
     */
    private $imageCard;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="project")
     */
    private $images;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getImageCard(): ?string
    {
        return $this->imageCard;
    }

    public function setImageCard(string $imageCard): self
    {
        $this->imageCard = $imageCard;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setProject($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getProject() === $this) {
                $image->setProject(null);
            }
        }

        return $this;
    }
}
